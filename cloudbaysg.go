package main

import (
	"bufio"
	"database/sql"
	"fmt"
	"os"

	_ "github.com/mattn/go-sqlite3"
)

const dbPath string = "/home/campus31/nschultz/projects/cloudbaysg/cloudbaysg.db"

// ================== INIT FUNCTION ==================
func initialize() {
	database, _ := sql.Open("sqlite3", dbPath)

	listStatement, _ := database.Prepare("CREATE TABLE IF NOT EXISTS bays(id TEXT PRIMARY KEY, ticketID TEXT DEFAULT 0, taskID TEXT DEFAULT 0, name TEXT DEFAULT 'n/a', size TEXT NOT NULL, notes TEXT DEFAULT 'n/a')")
	listStatement.Exec()

}

// ================== LIST FUNCTION ==================
func list() {
	database, _ := sql.Open("sqlite3", dbPath)

	rows, _ := database.Query("SELECT id, ticketID, taskID, name, size, notes FROM bays")
	var id string
	var ticketID string
	var taskID string
	var name string
	var size string
	var notes string

	fmt.Println("\n--------------------------------\nBAY ENTRIES\n--------------------------------")

	for rows.Next() {
		rows.Scan(&id, &ticketID, &taskID, &name, &size, &notes)
		fmt.Printf("Bay ID: %v\tTicket ID: %-12v\tTask ID: %-12v\tLabel: %-15v\tSize: %v\tAdditional Notes: %v\n", id, ticketID, taskID, name, size, notes)
	}
	fmt.Print("\n")
}

// ================== ADD FUNCTION ==================
func add() {
	database, _ := sql.Open("sqlite3", dbPath)

	var id string
	var ticketID string
	var taskID string
	var name string
	var size string
	var notes string
	scanner := bufio.NewScanner(os.Stdin)

	fmt.Println("\n--------------------------------\nADDING BAY ENTRY\n--------------------------------")

	fmt.Print("Bay ID: ")
	scanner.Scan()
	id = scanner.Text()
	fmt.Print("Ticket ID (optional): ")
	scanner.Scan()
	ticketID = scanner.Text()
	fmt.Print("Task ID (optional): ")
	scanner.Scan()
	taskID = scanner.Text()
	fmt.Print("Drive Label (optional): ")
	scanner.Scan()
	name = scanner.Text()
	fmt.Print("Drive Size [MB|GB|T]: ")
	scanner.Scan()
	size = scanner.Text()
	fmt.Print("Additional Notes (optional): ")
	scanner.Scan()
	notes = scanner.Text()

	addStatement, _ := database.Prepare("INSERT INTO bays (id, ticketID, taskID, name, size, notes) VALUES (?, ?, ?, ?, ?, ?)")
	addStatement.Exec(id, ticketID, taskID, name, size, notes)
	fmt.Println("\nEntry Added!")
}

// ================== CLEAR FUNCTION ==================
func clear() {
	database, _ := sql.Open("sqlite3", dbPath)

	var id string
	scanner := bufio.NewScanner(os.Stdin)

	fmt.Println("\n--------------------------------\nCLEARING BAY ENTRY\n--------------------------------")

	fmt.Print("Bay ID: ")
	scanner.Scan()
	id = scanner.Text()

	clearStatement, _ := database.Prepare("DELETE FROM bays WHERE id=?")
	clearStatement.Exec(id)
	fmt.Println("\nEntry Cleared!")
}

// ================== EDIT FUNCTION ==================
func edit() {

	database, _ := sql.Open("sqlite3", dbPath)

	var id string
	var userField string
	var dbField string
	var newVal string
	scanner := bufio.NewScanner(os.Stdin)

	fmt.Println("\n--------------------------------\nEDITING BAY ENTRY\n--------------------------------")

	fmt.Print("Bay ID of Row to Edit: ")
	scanner.Scan()
	id = scanner.Text()
	fmt.Print("Field to Edit [id|ticket|task|label|size|notes]: ")
	scanner.Scan()
	userField = scanner.Text()
	fmt.Print("New Value for Selected Field: ")
	scanner.Scan()
	newVal = scanner.Text()

	if userField == "id" {
		dbField = "id"
	} else if userField == "ticket" {
		dbField = "ticketID"
	} else if userField == "task" {
		dbField = "taskID"
	} else if userField == "label" {
		dbField = "name"
	} else if userField == "size" {
		dbField = "size"
	} else if userField == "notes" {
		dbField = "notes"
	} else {
		dbField = "ERROR"
	}

	if dbField == "ERROR" {
		fmt.Println("\nERROR IN EDITING. EXITING.")
	} else {
		editStatement, _ := database.Prepare("UPDATE bays SET ?=? WHERE id=?")
		editStatement.Exec(dbField, newVal, id)
		fmt.Println("\nEntry Edited!")
	}

}

// ================== HELP FUNCTION ==================
func help() {

	fmt.Println("\nINFO:")
	fmt.Println("\tA non-google hosted way of handling what goes into the cloud. Stores and handles info on the drive bays located at thecloud.\n\tInformation is purely visual and does not affect the drive bays or thecloud in any physical way.")
	fmt.Println("\nUSAGE:")
	fmt.Println("\tcloudbaysg (initialize|init|i)\t\tInitializes table. This should be run first if the database is fresh\n\t\t\t\t\t\tand has never had data stored in it yet")
	fmt.Println("\tcloudbaysg (list|l)\t\t\tLists all current entries")
	fmt.Println("\tcloudbaysg (add|a)\t\t\tRuns the Add Entry prompt")
	fmt.Println("\tcloudbaysg (edit|e)\t\t\tRuns the Edit Entry prompt")
	fmt.Println("\tcloudbaysg (clear|c)\t\t\tRuns the Clear Entry prompt")
	fmt.Println("\tcloudbaysg (help|h)\t\t\tShows this help page")
	fmt.Println("")

}

// ================== MAIN FUNCTION ==================
func main() {

	if len(os.Args) == 1 {
		help()
	} else {
		switch os.Args[1] {
		case "add", "a":
			add()
		case "initialize", "i", "init":
			initialize()
		case "edit", "e":
			edit()
		case "list", "l":
			list()
		case "clear", "c":
			clear()
		case "help", "h":
			help()
		default:
			fmt.Println("\nBAD OPTION. EXITING.")
		}
	}

}
