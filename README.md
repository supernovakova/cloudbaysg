# CloudBaysG
A non-google and locally hosted way to handle what goes into thecloud written in Go.
Successor to CloudBays and ported to Go from CloudBays3. Created by Nick Schultz (nschultz@mtu.edu)

### REQUIREMENTS
CloudBaysG uses Go.
CloudBaysG requires `mattn/go-sqlite3` for sqlite3 driver initialization. It can be downloaded using the following go command:
```
[nschultz@argon ~]$ go get github.com/mattn/go-sqlite3
```
**Note for Windows Users:** You must have GCC with 64-bit support in order to successfully `go get` this package.
See [this link](https://jmeubank.github.io/tdm-gcc/) for an example GCC port for Windows with 64-bit included by default.

### SETUP
Before CloudbaysG can be used, two things must be done. First, it must first be built. The following will build `cloudbaysg.go` and create a binary on UNIX-like systems or an executable on Windows systems:
```
[nschultz@argon cloudbaysg]$ go build cloudbaysg.go
```
**Note:** it is possible to run CloudbaysG without building by using `go run` but for ease of distribution `go build` will be used here.
Second, the `cloudbaysgo.db` file must be created with the following command:
```
[nschultz@argon cloudbaysg]$ touch cloudbaysg.db
```
Then, the `cloudbaysg.go` file must be edited with the absolute path to this database file as the value of the db_path variable:
```
const db_path string = "/path/to/cloudbaysg.db"
```

### USAGE GUIDE
```
+---------------+          +--------------------------------------+
|  usb - bay 7  |          |           thecloud - bay 5           |
+---------------+          +--------------------------------------+
+---------------+          +--------------------------------------+
|  usb - bay 6  |          |           thecloud - bay 4           |
+---------------+          +--------------------------------------+
                           +--------------------------------------+
                           |           thecloud - bay 3           |
                           +--------------------------------------+
                           +--------------------------------------+
                           |           thecloud - bay 2           |
                           +--------------------------------------+
                           +--------------------------------------+
                           |           thecloud - bay 1           |
                           +--------------------------------------+
```

The following will create an entry for a 500GB drive labeled "testdrive" at bay 1 with request ID of 1234 and task ID of 4321 with "zfs recovery" as additional notes:
```
[nschultz@argon ~]$ ./cloudbaysg add

----------------------------
ADDING BAY ENTRY
----------------------------

Bay ID: 1
Ticket ID (optional): 1234
Task ID (optional): 4321
Drive Label (optional): testdrive
Drive Size [MB|GB|T]: 500GB
Additional notes (optional): zfs recovery

Entry Added!
```
The following will list all entries in CloudBaysG:
```
[nschultz@argon ~]$ ./cloudbaysg list

----------------------------
BAY ENTRIES
----------------------------

Bay ID: 1   Ticket ID: 1234     Task ID: 4321       Label: testdrive       Size: 500GB      Notes: zfs recovery
```
The following will clear the entry at bay 1:
```
[nschultz@argon ~]$ ./cloudbaysg clear

----------------------------
CLEARING BAY ENTRY
----------------------------

Bay ID: 1

Entry Cleared!
```
